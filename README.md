# PROYECTO FINAL FUNDAMENTOS DE ALGORITMIA

## Enunciado
La empresa de productos de belleza MaryQ requiere una aplicación para administrar la información referente a todas sus tiendas a nivel nacional.   
kkkkkk

En Colombia, por ciudad hay mínimo una sede física  y máximo 3.  

Cada sede tiene un administrador y hay uno a nivel nacional. 

Por sedes  hay un listado de  productos que se clasifican en la siguientes categorías: 

* Cuidado facial
* Cuidado corporal
* Fragancias
* Maquillaje
* Capilar.  

Todos estos productos encajan dentro de las categorías: 
* Hombre
* Mujer 
* Bebés y niños.

De cada administrador se cuenta con información tal  como:
* id
* nombre
* dirección
* correo electrónico
* fecha de nacimiento
* estudios realizados. 

A nivel nacional hay un listado sin repeticiones de todos los productos disponibles, igual ocurre por cada sede  y por cada ciudad.

Los productos disponibles  pueden ser adquiridos por los  clientes, siempre y cuando se encuentren disponibles, en la ciudad en donde él lo solicita y que el cliente esté registrado en el sistema. 

Del cliente se tiene: 

* id
* nombre
* dirección
* correo electrónico
* fecha de nacimiento
* ciudad
* departamento de residencia. 

Si un producto es solicitado por un cliente y no hay existencias del mismo en su ciudad, podrá consultar a nivel nacional para determinar en dónde está disponible. 

Para que los productos lleguen a la casa del cliente, éste debe pagar el envío por una empresa de mensajería, bien sea a nivel departamental o nacional. El pago deberá hacerlo con tarjeta de crédito o débito.   

Los datos de los pagos deben ser cuidadosamente almacenados por el sistema.

## Acciones del sistema

Desde la vista de administrador el sistema deberá realizar lo siguiente:

* El administrador podrá aumentar o disminuir la cantidad de existencias de los productos existentes en la tienda. También podrá agregar nuevos productos.
* El sistema debe enviar un correo electrónico al administrador de la sede, si hay un producto del que queden menos de 10 existencias, para solicitar y aumentar las mismas. Si hay muchos productos con cantidad de existencias bajas, todo debe enviarse en un único correo. Esta revisión solo la hace el sistema los días lunes. Usar Calendario Gregoriano para ello.
* Obtener un listado ordenado, con los clientes que tengan compras acumuladas en el rango de precio (mínimo y máximo) y fecha (fecha inicial, fecha final) indicado por el administrador (usar Calendar). El listado podrá ordenarse por acumulado o alfabéticamente. El listado es diferente si lo solicita el administrador general o el de la sede.
* Obtener un listado con los productos más vendidos  (por cantidad de existencia) en el rango de tiempo indicado.
* Obtener el cliente que realizó la mayor compra del mes y enviarle un regalo de la tienda seleccionado aleatoriamente, de acuerdo a su género, siempre y cuando no exceda los 20000.
* Obtener consolidado del dinero recaudado por mes, periodo y año.
* Identificar las 5 sedes con mayores ventas.
* Para una determinada sede, dando una fecha de inicio y una fecha final, informar el día en el que  se han realizado compras más costosas.
* Crear intervalos por diferentes edades e informar por cada uno de los intervalos, cuál es el producto más vendido
* Mostrar el consolidado de dinero por sede y  a nivel nacional. 

 
Desde la vista del cliente el sistema deberá realizar lo siguiente:

* Efectuar compras, una compra puede tener  1 o más productos. El pago solo  puede hacerse con tarjeta crédito o débito. Si el pago es aprobado llegará a su email un correo electrónico, con el detalle de la factura.
* Visualizar su histórico de compras (ordenado de más reciente a menos reciente)
* Seguir información de un pedido, que aún no ha llegado a su vivienda 
* Crear un reclamo, si lo que llegó a la vivienda no es lo solicitado, no cumple con lo prometido o no ha llegado.
* Tener un listado de productos favoritos, para facilitar su compra la  próxima vez.

Para garantizar la salud de los clientes, MaryQ no acepta devoluciones de productos.

Debe construir múltiples ventanas con JavaFX, usar TableView,  ListView, Menu. Además de los componentes presentes en el tutorial entregado para construir interfaz gráfica.
Debe usar persistencia para almacenar los datos.

El monitor del espacio académico lo estará guiando para resolver inquietudes y ayudarles a resolver errores de código.
El proyecto final se calificará de la siguiente forma:
Código: 0 a 5
Sustentación de carácter individual: 0 a 1 (con 2 decimales)
Nota final del proyecto: Código* Sustentación
La sustentación corresponde a la adición o modificación de métodos acorde al código construido. 

por jose cordoba 
pablo 
Steven 
samuel 