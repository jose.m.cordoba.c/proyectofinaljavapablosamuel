package co.uniquindio.address;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.UUID;
import java.util.logging.Handler;

import co.uniquindio.address.model.Administrador;
import co.uniquindio.address.model.Cliente;
import co.uniquindio.address.model.Cuidad;
import co.uniquindio.address.model.Empresa;
import co.uniquindio.address.model.Producto;
import co.uniquindio.address.model.Sede;
import co.uniquindio.address.model.Enum.TipoEstudio;
import co.uniquindio.address.view.ControladorAdministrador;
import co.uniquindio.address.view.ControladorAdministradorNacional;
import co.uniquindio.address.view.ControladorCuidad;
import co.uniquindio.address.view.ControladorEntrada;
import co.uniquindio.address.view.ControladorNuevaCuidad;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

public class Principal extends Application {
	private Stage escenarioPrincipal;
	private BorderPane layoutRaiz;
	private Empresa mariQ;
	public final String RUTA="empresa.ser";

	@Override
	public void start(Stage primaryStage) {
		mariQ = cargarEmpresa(this.RUTA);

		this.escenarioPrincipal = primaryStage;
		this.escenarioPrincipal.setTitle("MariQ");
		this.escenarioPrincipal.setOnCloseRequest(e -> handleExit());
		inicializarLayoutRaiz();
		mostrarVistaEntrada();
	}

	public Empresa getMariQ() {
		return mariQ;
	}

	public void setMariQ(Empresa mariQ) {
		this.mariQ = mariQ;
	}

	/**
	 * Inicializa el layout raiz
	 */
	public void inicializarLayoutRaiz() {
		try {
			// Carga el root layout desde un archivo xml
			FXMLLoader cargador = new FXMLLoader();
			cargador.setLocation(Principal.class.getResource("view/LayoutRaiz.fxml"));
			layoutRaiz = (BorderPane) cargador.load();
			// Muestra la escena que contiene el RootLayout
			Scene scene = new Scene(layoutRaiz);
			escenarioPrincipal.setScene(scene);
			escenarioPrincipal.show();
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}
	}

	/**
	 * Shows the person overview inside the root layout.
	 */
	public void mostrarVistaEntrada() {
		try {
			// Carga la vista de la persona.
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(Principal.class.getResource("view/vistaEntrada.fxml"));
			AnchorPane vistaGeneral = (AnchorPane) loader.load();
			// Fija la vista de la person en el centro del root layout.
			layoutRaiz.setCenter(vistaGeneral);
			// Acceso al controlador.
			ControladorEntrada miControlador = loader.getController();
			miControlador.setMiVentanaPrincipal(this);
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}
	}

	/**
	 * Shows the person overview inside the root layout.
	 */

	public Empresa cargarEmpresa(String ruta) {
		Empresa retorno;
		try {
			FileInputStream fis;
			fis = new FileInputStream(ruta);
			ObjectInputStream ois = new ObjectInputStream(fis);
			retorno = (Empresa) ois.readObject();
			ois.close();
			fis.close();
			System.out.println("Empresa Cargada"+" "+retorno);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			System.out.println("no hay archivo");
			retorno = new Empresa(" MariQ ", new Administrador(UUID.randomUUID(), "Jose", " Calle falsa 123",
					" jose@ejemplo.com", Calendar.getInstance(), TipoEstudio.superior));
		}
		return retorno;
	}

	private void handleExit() {
		// handle disposing of the timestamp thread
		guardarEmpresa(mariQ,this.RUTA);
		Platform.exit();
		System.exit(0);
	}

	public void guardarEmpresa(Empresa mariQ2, String string) {
		try {
			FileOutputStream fos = new FileOutputStream(string);
			ObjectOutputStream oos = new ObjectOutputStream(fos);
			oos.writeObject(mariQ2);
			oos.close();
			fos.close();
			System.out.println("Empresa guardada"+" "+mariQ2);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 *
	 * @return
	 */

	public Stage getEscenarioPrincipal() {
		return escenarioPrincipal;
	}

	public void setEscenarioPrincipal(Stage escenarioPrincipal) {
		this.escenarioPrincipal = escenarioPrincipal;
	}

	public static void main(String[] args) {

		launch(args);
	}

	public ArrayList<Producto> buscarListaProductos() {
		return mariQ.getProductos();
	}

	public ArrayList<Sede> buscarListaSede() {
		return mariQ.getSedes();
	}

	public ArrayList<Cliente> buscarListaCliente() {
		return mariQ.getClientes();
	}

	public ArrayList<Cuidad> buscarListaCuidad() {
		return mariQ.getCuidades();
	}

}