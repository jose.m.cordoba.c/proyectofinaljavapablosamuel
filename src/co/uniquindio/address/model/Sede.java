package co.uniquindio.address.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;

public class Sede implements Serializable{
	private String nombreT;
	private Administrador administradorL;
	private ArrayList<Producto> inventarioS;
	private ArrayList<Cliente> listaClientes;
	private ArrayList<Compra> listaDeCompras;

	public ArrayList<Cliente> getListaClientes() {
		return listaClientes;
	}

	public ArrayList<Cliente> obtenerComprasAcumuladas(int precioMinimo, int precioMaximo, 
			Calendar fechaInicial, Calendar fechaFinal) throws Exception{
		// TODO Implementar ESTO
		throw new Exception("No se ha implementado");
	}
	
	
	public void setListaClientes(ArrayList<Cliente> listaClientes) {
		this.listaClientes = listaClientes;
	}

	public String getNombreT() {
		return nombreT;
	}

	public void setNombreT(String nombreT) {
		this.nombreT = nombreT;
	}

	public Administrador getAdministradorL() {
		return administradorL;
	}

	public void setAdministradorL(Administrador administradorL) {
		this.administradorL = administradorL;
	}

	public ArrayList<Producto> getListaProductos() {
		return inventarioS;
	}

	public void setListaProductos(ArrayList<Producto> listaProductos) {
		this.inventarioS = listaProductos;
	}

	public ArrayList<Compra> getListaDeCompras() {
		return listaDeCompras;
	}

	public void setListaDeCompras(ArrayList<Compra> listaDeCompras) {
		this.listaDeCompras = listaDeCompras;
	}

	

	public Sede(String nombreT, Administrador administradorL) {
		this.nombreT = nombreT;
		this.administradorL = administradorL;
		this.inventarioS= new ArrayList<Producto>();
		this.listaClientes= new ArrayList<Cliente>();
		this.listaDeCompras= new ArrayList<Compra>();
	}

	public ArrayList<Producto> notificarMenosDe10Existencias() {
		ArrayList<Producto> listaProducto = new ArrayList<Producto>();
		for (int i = 0; i < inventarioS.size(); i++) {
			if (inventarioS.get(i).getExistencias() < 10) {
				listaProducto.add(inventarioS.get(i));
			}
		}
		return listaProducto;
	}

	public void agregarProducto(Producto producto) throws Exception {
		if (!inventarioS.contains(producto)) {
			inventarioS.add(producto);
		} else {
			throw new Exception("Este producto ya existe");
		}
	}

	public void disminuirProducto(Producto producto, int cantidad) throws Exception {

		if (inventarioS.contains(producto)) {
			int indice = inventarioS.indexOf(producto);// obtiene la pocion del producto
			int existencias = inventarioS.get(indice).getExistencias();
			if (existencias >= cantidad) {
				inventarioS.get(indice).setExistencias(existencias - cantidad);
			} else {
				inventarioS.get(indice).setExistencias(0);
			}
		} else {
			throw new Exception("Este preducto no existe");
		}
	}

	public void aumentarProducto(Producto producto, int cantidad) throws Exception {
		if (inventarioS.contains(producto)) {
			int indice = inventarioS.indexOf(producto);// obtiene la pocion del producto
			int existencias = inventarioS.get(indice).getExistencias();
			inventarioS.get(indice).setExistencias(existencias + cantidad);
		} else {
			throw new Exception("Este preducto no existe");
		}

	}

	@Override
	public String toString() {
		return "Sede [nombreT=" + nombreT + ", administradorL=" + administradorL + ", inventarioS=" + inventarioS
				+ ", listaClientes=" + listaClientes + ", listaDeCompras=" + listaDeCompras + "]";
	}
	
	

}
