/**
 * 
 */
package co.uniquindio.address.model;

import java.util.Calendar;
import java.util.UUID;

/**
 * @author jose
 *
 */
public class Cliente extends Persona {

	//TODO Cuadrar los atributos de la clase
	private String departamento, ciudad;
	private Tarjeta tarjeta;
	
	/**
	 * @param id
	 * @param nombre
	 * @param direccion
	 * @param correo
	 * @param fecha_nacimiento
	 */
	
	public Cliente(UUID id, String nombre, String direccion, String correo, Calendar fecha_nacimiento,
			String departamento, String ciudad, Tarjeta tarjeta) {
		super(id, nombre, direccion, correo, fecha_nacimiento);
		this.departamento = departamento;
		this.ciudad = ciudad;
		this.tarjeta = tarjeta;
	}

	public String getDepartamento() {
		return departamento;
	}

	public void setDepartamento(String departamento) {
		this.departamento = departamento;
	}

	public String getCiudad() {
		return ciudad;
	}

	public void setCiudad(String ciudad) {
		this.ciudad = ciudad;
	}

	public Tarjeta getTarjeta() {
		return tarjeta;
	}

	public void setTarjeta(Tarjeta tarjeta) {
		this.tarjeta = tarjeta;
	}

	@Override
	public String toString() {
		return "Cliente [departamento=" + departamento + ", ciudad=" + ciudad + ", tarjeta=" + tarjeta + "]";
	}
	
	
	

}
