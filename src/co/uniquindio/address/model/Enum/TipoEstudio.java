package co.uniquindio.address.model.Enum;

public enum TipoEstudio {
	basica(0), superior (1);
	
	private int numTipo;

	private TipoEstudio(int numTipo) {
		this.numTipo = numTipo;

	}

	public int getNumTipo() {
		return numTipo;
		}
 	
 	public String toString() {
 		String Tipo="";
 		switch (numTipo) {
 		
 		case 0:Tipo="basica";
 		break;
 		
 		case 1:Tipo="superior";
 		break;
 				
 		}
 	return Tipo;
 	}

}
