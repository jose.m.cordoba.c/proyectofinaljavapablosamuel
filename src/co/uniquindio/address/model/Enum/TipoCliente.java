package co.uniquindio.address.model.Enum;

public enum TipoCliente {
	hombre(0), mujer (1), bebesYNinos(2);
	
	private int numTipo;

	private TipoCliente(int numTipo) {
		this.numTipo = numTipo;

	}

	public int getNumTipo() {
		return numTipo;
		}
 	
 	public String toString() {
 		String Tipo="";
 		switch (numTipo) {
 		
 		case 0:Tipo="hombre";
 		break;
 		
 		case 1:Tipo="mujer";
 		break;
 		
 		case 2:Tipo="bebesYNinos";
 		break;
 				
 		}
 	return Tipo;
 	}
 	
}

