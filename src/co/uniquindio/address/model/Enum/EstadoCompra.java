package co.uniquindio.address.model.Enum;

public enum EstadoCompra {

	inexsistente (8), disponible (9), pagado (10), sinpagar (11);
	
	private int numEstado;
	
	private EstadoCompra(int numEstado) {
		this.numEstado= numEstado;
	}
	
	
	public int getnumTipo() {
		return numEstado;
	}
	public String toString() {
		String Estado="";
		
		switch (numEstado) {
			
			case 8:Estado="inexistente";
			break;
			
			case 9:Estado="disponible";
			break;
			
			case 10:Estado="pagado";
			break;
			
			case 11:Estado="sinpagar";
			break;
			
		}
		return Estado;
			
		}
	}

