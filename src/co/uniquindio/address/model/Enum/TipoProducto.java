package co.uniquindio.address.model.Enum;

public enum TipoProducto {
	cuidadoFacial(0), cuidadoCorporal (1), fragancias(2),maquillaje(3),capilar(4);
	
	private int numTipo;

	private TipoProducto(int numTipo) {
		this.numTipo = numTipo;

	}

	public int getNumTipo() {
		return numTipo;
		}
 	
 	public String toString() {
 		String Tipo="";
 		switch (numTipo) {
 		
 		case 0:Tipo="cuidadoFacial";
 		break;
 		
 		case 1:Tipo="cuidadoCorporal";
 		break;
 		
 		case 2:Tipo="fragancias";
 		break;
 		
 		case 3:Tipo="maquillaje";
 		break;
 		
 		case 4:Tipo="capilar";
 		break;
 				
 		}
 	return Tipo;
 	}
 	
}