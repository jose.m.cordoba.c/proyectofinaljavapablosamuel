package co.uniquindio.address.model.Enum;

public enum TipoTarjeta {
    debito(0), credito (1);
	
	private int numTipo;

	private TipoTarjeta(int numTipo) {
		this.numTipo = numTipo;

	}

	public int getNumTipo() {
		return numTipo;
		}
 	
 	public String toString() {
 		String Tipo="";
 		switch (numTipo) {
 		
 		case 0:Tipo="debito";
 		break;
 		
 		case 1:Tipo="credito";
 		break;
 				
 		}
 	return Tipo;
 	}
 	
}