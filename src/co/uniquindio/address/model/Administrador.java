/**
 * 
 */
package co.uniquindio.address.model;

import java.util.Calendar;
import java.util.UUID;

import co.uniquindio.address.model.Enum.TipoEstudio;

/**
 * @author jose
 *
 */
public class Administrador extends Persona {

	//TODO Cuadrar este metodo
	private TipoEstudio tipoEstudio;
	
	/**
	 * @param id
	 * @param nombre
	 * @param direccion
	 * @param correo
	 * @param fecha_nacimiento
	 */
	public Administrador(UUID id, String nombre, String direccion, String correo, Calendar fecha_nacimiento, TipoEstudio tipoEstudio) {
		super(id, nombre, direccion, correo, fecha_nacimiento);
		this.tipoEstudio=tipoEstudio;
		// TODO Auto-generated constructor stub
	}

	public TipoEstudio getTipoEstudio() {
		return tipoEstudio;
	}

	public void setTipoEstudio(TipoEstudio tipoEstudio) {
		this.tipoEstudio = tipoEstudio;
	}

	@Override
	public String toString() {
		return "Administrador [tipoEstudio=" + tipoEstudio + "]";
	}

  

}
