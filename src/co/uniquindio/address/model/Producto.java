package co.uniquindio.address.model;

import java.io.Serializable;

import co.uniquindio.address.model.Enum.TipoCliente;
import co.uniquindio.address.model.Enum.TipoProducto;

public class Producto implements Serializable{

	
	/*Atributos*/
	private TipoCliente cliente;
	private TipoProducto tipo;
	private String nombre;
	private int precio;
	private int existencias; 
	
	/**
	 * Construye un nuevo Producto
	 * 
	 * @param cliente
	 * @param tipo
	 * @param nombre
	 * @param precio
	 * @param existencias
	 */
	public Producto(TipoCliente cliente, TipoProducto tipo, String nombre, int precio, int existencias) {
		this.setCliente(cliente);
		this.setTipo(tipo);
		this.setNombre(nombre);
		this.setPrecio(precio);
		this.setExistencias(existencias);
	}

	/**
	 * Obtiene la categoría
	 * @return
	 */
	TipoCliente getcliente() {
		return cliente;
	}

	void setCliente(TipoCliente cliente) {
		this.cliente = cliente;
	}

	TipoProducto getTipo() {
		return tipo;
	}

	void setTipo(TipoProducto tipo) {
		this.tipo = tipo;
	}

	String getNombre() {
		return nombre;
	}

	void setNombre(String nombre) {
		this.nombre = nombre;
	}

	int getExistencias() {
		return existencias;
	}

	void setExistencias(int existencias) {
		this.existencias = existencias;
	}

	int getPrecio() {
		return precio;
	}

	void setPrecio(int precio) {
		this.precio = precio;
	}

	@Override
	public String toString() {
		return "Producto [cliente=" + cliente + ", tipo=" + tipo + ", nombre=" + nombre + ", precio=" + precio
				+ ", existencias=" + existencias + "]";
	}
	
	
	
    
}
