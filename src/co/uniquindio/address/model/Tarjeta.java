package co.uniquindio.address.model;

import java.io.Serializable;
import java.util.UUID;

import co.uniquindio.address.model.Enum.TipoTarjeta;

public class Tarjeta implements Serializable{
	
	
	
	private UUID tarjetaDeCredito;
    private TipoTarjeta tipoTarjetaC;
	    
    public Tarjeta(UUID tarjetaDeCredito, TipoTarjeta tipoTarjetaC) {
		this.tarjetaDeCredito = tarjetaDeCredito;
		this.tipoTarjetaC = tipoTarjetaC;
	}
	
    public UUID getTarjetaDeCredito() {
		return tarjetaDeCredito;
	}
	public void setTarjetaDeCredito(UUID tarjetaDeCredito) {
		this.tarjetaDeCredito = tarjetaDeCredito;
	}
	public TipoTarjeta getTipoTarjetaC() {
		return tipoTarjetaC;
	}
	public void setTipoTarjetaC(TipoTarjeta tipoTarjetaC) {
		this.tipoTarjetaC = tipoTarjetaC;
	}

	@Override
	public String toString() {
		return "Tarjeta [tarjetaDeCredito=" + tarjetaDeCredito + ", tipoTarjetaC=" + tipoTarjetaC + "]";
	}
    
	
	
}
     