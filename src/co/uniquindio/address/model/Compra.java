package co.uniquindio.address.model;

import java.util.Calendar;
import java.io.Serializable;
import java.util.ArrayList;

public class Compra implements Serializable{

    private ArrayList<Producto> productos;
    private int[] cantidades;
    private boolean estadoPago;
    private Cliente cliente;
    private int estadoEnvio;
    private Tarjeta tarjeta;
    private Calendar fecha;
    private String reclamo;
    private double valorCompra;

    /**
     *
     * @param productos
     * @param cantidades
     * @param cliente
     * @param tarjeta
     */
    public Compra(ArrayList<Producto> productos, int[] cantidades, Cliente cliente, Tarjeta tarjeta) {
        this.cantidades = cantidades;
        this.productos = productos;
        this.estadoPago = false;
        this.cliente = cliente;
        this.estadoEnvio = 0;
        this.tarjeta = tarjeta;
        this.fecha = Calendar.getInstance();
        this.reclamo = "";
        calcularValorCompra();
    }


    public double getValorCompra() {
        return valorCompra;
    }

    public void setValorCompra(double valorCompra) {
        this.valorCompra = valorCompra;
    }

    public ArrayList<Producto> getProductos() {
        return productos;
    }

    public boolean estaPagada() {
        return estadoPago;
    }

    /**
     *
     * @return
     */
    private boolean validarExistencias(){
        for (int i = 0; i < productos.size(); i++) {
            int existencias = productos.get(i).getExistencias();
            int cantidad = cantidades[i];
            if (existencias < cantidad){
                return false;
            }
        }
        return true;
    }

    /**
     *
     * @throws Exception
     */
    public void pagar() throws Exception {
        if(!validarExistencias()){
            throw new Exception("Los productos no estan disponibles");
        }
        for (int i = 0; i < productos.size(); i++) {
            int existencias = productos.get(i).getExistencias();
            int cantidad = cantidades[i];
            productos.get(i).setExistencias(existencias - cantidad);
        }
        this.estadoPago = true;
    }

    /**
     *
     */
    private void calcularValorCompra(){
        for (int i = 0; i < productos.size(); i++) {
            int cantidad = cantidades[i];
            this.valorCompra += cantidad * productos.get(i).getPrecio();
        }
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public int getEstadoEnvio() {
        return estadoEnvio;
    }

    public void setEstadoEnvio(int estadoEnvio) {
        this.estadoEnvio = estadoEnvio;
    }

    public Tarjeta getTarjeta() {
        return tarjeta;
    }

    public void setTarjeta(Tarjeta tarjeta) {
        this.tarjeta = tarjeta;
    }

    public Calendar getFecha() {
        return fecha;
    }

    public void setFecha(Calendar fecha) {
        this.fecha = fecha;
    }

    public String getReclamo() {
        return reclamo;
    }

    public void setReclamo(String reclamo) {
        this.reclamo = reclamo;
    }
}