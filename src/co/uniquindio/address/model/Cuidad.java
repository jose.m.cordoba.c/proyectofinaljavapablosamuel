package co.uniquindio.address.model;

import java.io.Serializable;
import java.util.ArrayList;

public class Cuidad implements Serializable{

	private String nombreC;
	private ArrayList<Sede> sedes;

	public String getNombresC() {
		return nombreC;
	}

	public void setNombresC(String nombresC) {
		this.nombreC = nombresC;
	}

	public Cuidad(String nombreC) {
		this.sedes = new ArrayList<Sede>();
		this.nombreC=nombreC;
	}

	public void agregarSede(Sede sede) throws Exception {
		if (sedes.size() < 3) {
			sedes.add(sede);
		}
		else {
			throw new Exception("No se puede tener mas de 3 sedes por cuidad");
		}
	}

	public ArrayList<Sede> obtenerSedes() {
		return sedes;
	}

	@Override
	public String toString() {
		return "Cuidad [nombreC=" + nombreC + ", sedes=" + sedes + "]";
	}

	
	
}
