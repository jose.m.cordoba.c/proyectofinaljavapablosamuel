package co.uniquindio.address.model;

import java.io.Serializable;
import java.util.ArrayList;

public class Empresa implements Serializable {

	private String nombreEmpresa;
	private Administrador administradorNacional;
	private ArrayList<Administrador> administradoresSedes;
	private ArrayList<Sede> sedes;
	private ArrayList<Cuidad> cuidades;
	private ArrayList<Cliente> clientes;
	private ArrayList<Producto> productos;

	public String getNombreEmpresa() {
		return nombreEmpresa;
	}

	public void setNombreEmpresa(String nombreEmpresa) {
		this.nombreEmpresa = nombreEmpresa;
	}

	public Administrador getAdministradorNacional() {
		return administradorNacional;
	}

	public void setAdministradorNacional(Administrador administradorNacional) {
		this.administradorNacional = administradorNacional;
	}

	public ArrayList<Administrador> getAdministradoresSedes() {
		return administradoresSedes;
	}

	/*
	 * de la linea 7 a la linea 88 secion de private y getters and setters/
	 */
	public ArrayList<Sede> getSedes() {
		return sedes;
	}

	public ArrayList<Cuidad> getCuidades() {
		return cuidades;
	}

	public ArrayList<Cliente> getClientes() {
		return clientes;
	}

	public ArrayList<Producto> getProductos() {
		return productos;
	}

	public Empresa(String nombreEmpresa, Administrador administradorNacional) {
		this.nombreEmpresa = nombreEmpresa;
		this.administradorNacional = administradorNacional;
		this.cuidades = new ArrayList<Cuidad>();
		this.clientes = new ArrayList<Cliente>();
		this.productos = new ArrayList<Producto>();
		this.administradoresSedes = new ArrayList<Administrador>();
		this.sedes = new ArrayList<Sede>();
	}
	
	

	@Override
	public String toString() {
		return "Empresa [nombreEmpresa=" + nombreEmpresa + ", administradorNacional=" + administradorNacional
				+ ", administradoresSedes=" + administradoresSedes + ", sedes=" + sedes + ", cuidades=" + cuidades
				+ ", clientes=" + clientes + ", productos=" + productos + "]";
	}

	public void agregarAdministradorSede(Sede sede, Administrador administrador) {
		if (!administradoresSedes.contains(administrador)) {
			administradoresSedes.add(administrador);
		}
		sede.setAdministradorL(administrador);
	}

	public void agregarSede(Sede sede, Cuidad cuidad) throws Exception {
		if (!cuidades.contains(cuidad)) {
			cuidades.add(cuidad);
		}
		cuidad.agregarSede(sede);
		sedes.add(sede);
	}

	public void agregarCliente(Cliente cliente) throws Exception {
		if (!clientes.contains(cliente)) {
			clientes.add(cliente);
		}
	}

	public void agregarProducto(Producto producto, Sede sede) throws Exception {
		if (!productos.contains(producto)) {
			productos.add(producto);
		}
		sede.agregarProducto(producto);
	}

	public void agregarCuidad(Cuidad cuidad) {
		if (!cuidades.contains(cuidad)) {
			cuidades.add(cuidad);
		}
	}

	public void obtenerSedesConMasVentas() {

	}
}
