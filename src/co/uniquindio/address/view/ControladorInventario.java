package co.uniquindio.address.view;
import java.awt.Button;
import java.io.IOException;
import co.uniquindio.address.Principal;
import co.uniquindio.address.model.Cliente;
import co.uniquindio.address.model.Producto;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.fxml.FXML;
import javafx.scene.control.TextField;

public class ControladorInventario {
	    
	    @FXML
	    private TableColumn<Producto, Integer> ColumnaCantidad;

	    @FXML
	    private TableColumn<Producto, Boolean> ColumanTipo;

	    @FXML
	    private TableColumn<Producto, Boolean> columanCategoriaProducto;

	    @FXML
	    private TableView<Producto> tablaProductos;
	    private ObservableList<Producto> Producto ;

	    private Principal ventanaPrincipal;
	    private	Stage dialogStage;
		private Stage stagePrincipal;

		private Principal miVentanaPrincipal;
	    
	    public void inicializar () {
	    	Producto = FXCollections.observableArrayList();
	    	for(Producto productos :ventanaPrincipal.buscarListaProductos()) {
	    		Producto.add(productos);
	    	}
	    	this.ColumnaCantidad.setCellValueFactory(new PropertyValueFactory("Cantidad"));
	    	this.ColumanTipo.setCellValueFactory(new PropertyValueFactory("Tipo"));
	    	this.columanCategoriaProducto.setCellValueFactory(new PropertyValueFactory("Categoria Producto"));
	    	tablaProductos.setItems(Producto);
	    }
	    
	    @FXML
	    void AgregarProducto(MouseEvent event) {
	    	FXMLLoader loader = new FXMLLoader(this.getClass().getResource("/co/uniquindio/address/view/vistaNuevoProducto.fxml"));
			try {
				ControladorNuevosProductos control;
				Parent root = loader.load();
				control = loader.getController();
	            control.setMainApp(miVentanaPrincipal);
				Scene scene = new Scene(root);
				Stage stage = new Stage();
				stage.initModality(Modality.APPLICATION_MODAL);
				stage.setScene(scene);
				stage.show();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	    }

	    
	    @FXML
	    void actualizar(MouseEvent event) {

	    }

	    @FXML
	    void volver(MouseEvent event) {
	    	FXMLLoader loader = new FXMLLoader(this.getClass().getResource("/co/uniquindio/address/view/vistaAdministradorNacional.fxml"));
			try {
				ControladorAdministradorNacional control;
				Parent root = loader.load();
				control = loader.getController();
	            control.setMiVentanaPrincipal(miVentanaPrincipal);
				Scene scene = new Scene(root);
				Stage stage = new Stage();
				stage.initModality(Modality.APPLICATION_MODAL);
				stage.setScene(scene);
				stage.show();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	    }
		

	    public void setMainApp(Principal controladorCuidad) {
		       this.miVentanaPrincipal=	controladorCuidad;
		}

	}

