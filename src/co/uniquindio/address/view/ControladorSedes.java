package co.uniquindio.address.view;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.ResourceBundle;
import java.util.UUID;

import javax.swing.JOptionPane;

import co.uniquindio.address.Principal;
import co.uniquindio.address.model.Administrador;
import co.uniquindio.address.model.Cuidad;
import co.uniquindio.address.model.Empresa;
import co.uniquindio.address.model.Sede;
import co.uniquindio.address.model.Enum.TipoEstudio;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class ControladorSedes implements Initializable {

	@FXML
	private TextField nombreTextField;

	@FXML
	private ComboBox<String> cuidadCombo;

	private ObservableList<String> cuidades = FXCollections.observableArrayList();

	@FXML
	private TextField correoTextField;

	@FXML
	private ComboBox formacionDelAdministrador;

	@FXML
	private TextField dirrecciondTextField;

	@FXML
	private DatePicker fechaEscogida;
	
    @FXML
    private TextField nombreSedeTextField;

	private Principal ventanaPrincipal;

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		formacionDelAdministrador.getItems().removeAll(formacionDelAdministrador.getItems());
		formacionDelAdministrador.getItems().addAll("Educacion basica", "Educacion superior");
		formacionDelAdministrador.getSelectionModel().select("Educacion basica");
	}

	@FXML
	void actualizarCuides(MouseEvent event) {
		ArrayList<Cuidad> Cuidades = ventanaPrincipal.getMariQ().getCuidades();
		for (int i = 0; i < Cuidades.size(); i++) {
			cuidades.add(Cuidades.get(i).getNombresC());
		}
		cuidadCombo.setItems(cuidades);
	}

	@FXML
	void agregarSede(MouseEvent event) {
		int cuiudadSelecionada = cuidadCombo.getSelectionModel().getSelectedIndex();
		Cuidad modeloCuidad = ventanaPrincipal.getMariQ().getCuidades().get(cuiudadSelecionada);
		Calendar fecha = Calendar.getInstance();
		fecha.clear();
		fecha.set(fechaEscogida.getValue().getYear(), fechaEscogida.getValue().getMonthValue() - 1,
				fechaEscogida.getValue().getDayOfMonth());
		Administrador nuevoAdministrador = new Administrador(UUID.randomUUID(), nombreTextField.getText(),
				dirrecciondTextField.getText(), correoTextField.getText(), fecha,
				formacionDelAdministrador.getSelectionModel().getSelectedIndex() == 0 ? TipoEstudio.basica
						: TipoEstudio.superior);
		Sede nuevaSede = new Sede(nombreSedeTextField.getText(), nuevoAdministrador);
		try {
			Empresa maryQ = ventanaPrincipal.getMariQ();
			maryQ.agregarSede(nuevaSede, modeloCuidad);
			maryQ.agregarAdministradorSede(nuevaSede, nuevoAdministrador);
			this.ventanaPrincipal.guardarEmpresa(this.ventanaPrincipal.getMariQ(), this.ventanaPrincipal.RUTA);
			JOptionPane.showMessageDialog(null, "Se agrego la cuidad correctamete la sede" + " " + nuevaSede
					+ " y al administrador de la sede" + " " + nuevoAdministrador);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			JOptionPane.showMessageDialog(null, e.getMessage());
		}
	}

	@FXML
	void volver(MouseEvent event) {
		FXMLLoader loader = new FXMLLoader(this.getClass().getResource("/co/uniquindio/address/view/vistaCuidad.fxml"));
		try {
			ControladorCuidad control;
			Parent root = loader.load();
			control = loader.getController();
			control.setMiVentanaPrincipal(ventanaPrincipal);
			Scene scene = new Scene(root);
			Stage stage = new Stage();
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.setScene(scene);
			stage.show();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	
	public void setMiVentanaPrincipal(Principal miVentanaPrincipal) {
		this.ventanaPrincipal = miVentanaPrincipal;
	}
}
