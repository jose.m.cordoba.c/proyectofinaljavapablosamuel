package co.uniquindio.address.view;

import java.io.IOException;
import java.util.ArrayList;

import co.uniquindio.address.Principal;
import co.uniquindio.address.model.Cuidad;
import co.uniquindio.address.model.Producto;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.ComboBox;
import javafx.scene.input.MouseEvent;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class ControladorCompra {
	
	@FXML
    private ComboBox productosSede;
	private ObservableList Productos = FXCollections.observableArrayList();

	private Principal ventanaPrincipal;
 

	@FXML
	void actualizarCuides(MouseEvent event) {
		ArrayList<Producto> productos = ventanaPrincipal.getMariQ().getProductos();
		for (int i = 0; i < productos.size(); i++) {
			Productos.add(productos.get(i));
		}
		productosSede.setItems(Productos);
	}
	
    @FXML
    void realizarCompra(MouseEvent event) {

    }

    @FXML
    void volver(MouseEvent event) {
    	FXMLLoader loader = new FXMLLoader(this.getClass().getResource("/co/uniquindio/address/view/vistaCliente.fxml"));
		try {
			ControladorCliente control;
			Parent root = loader.load();
			control = loader.getController();
            control.setMiVentanaPrincipal(ventanaPrincipal);
			Scene scene = new Scene(root);
			Stage stage = new Stage();
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.setScene(scene);
			stage.show();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }

    public void setMiVentanaPrincipal(Principal miVentanaPrincipal) {
		this.ventanaPrincipal = miVentanaPrincipal;
	}

}
