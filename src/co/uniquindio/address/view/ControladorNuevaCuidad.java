package co.uniquindio.address.view;

import java.io.IOException;

import javax.swing.JOptionPane;

import co.uniquindio.address.Principal;
import co.uniquindio.address.model.Cuidad;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class ControladorNuevaCuidad {
	
	@FXML
    private TextField nombreCuidad;

  
	private Principal miVentanaPrincipal;

	@FXML
    void agregarCuidad(MouseEvent event) {
      String nombreCuidad=this.nombreCuidad.getText();
      Cuidad cuidad= new Cuidad(nombreCuidad);
      miVentanaPrincipal.getMariQ().agregarCuidad(cuidad);
      this.miVentanaPrincipal.guardarEmpresa(this.miVentanaPrincipal.getMariQ(), this.miVentanaPrincipal.RUTA);
      JOptionPane.showMessageDialog(null, "Se agrego la cuidad correctamete"+" "+cuidad);
	}

    public void setMiVentanaPrincipal(Principal principal) {
        this.miVentanaPrincipal=principal;			
		}


   
    @FXML
    void volver(MouseEvent event) {
    	FXMLLoader loader = new FXMLLoader(this.getClass().getResource("/co/uniquindio/address/view/vistaCuidad.fxml"));
		try {
			ControladorCuidad control;
			Parent root = loader.load();
			control = loader.getController();
			control.setMiVentanaPrincipal(miVentanaPrincipal);
			Scene scene = new Scene(root);
			Stage stage = new Stage();
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.setScene(scene);
			stage.show();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
