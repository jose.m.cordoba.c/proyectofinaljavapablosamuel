package co.uniquindio.address.view;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

import javax.swing.JOptionPane;

import co.uniquindio.address.Principal;
import co.uniquindio.address.model.Empresa;
import co.uniquindio.address.model.Producto;
import co.uniquindio.address.model.Sede;
import co.uniquindio.address.model.Enum.TipoCliente;
import co.uniquindio.address.model.Enum.TipoProducto;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class ControladorNuevosProductos implements Initializable {

	@FXML
	private TextField cantidadTextField;

	@FXML
	private TextField nombreTextField;

	@FXML
	private TextField precioTextField;

	@FXML
	private ComboBox<String> TipoComboBox;

	@FXML
	private ComboBox<String> categoriaComboBox;

	private Principal miVentanaPrincipal;

	@FXML
	private ComboBox<String> comboSedes;
	private ObservableList<String> ListaSedes = FXCollections.observableArrayList();;

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		TipoComboBox.getItems().removeAll(TipoComboBox.getItems());
		TipoComboBox.getItems().addAll("Hombre ", "Mujer", "Bebes y ni�os");
		TipoComboBox.getSelectionModel().select("Hombre");
		categoriaComboBox.getItems().removeAll(categoriaComboBox.getItems());
		categoriaComboBox.getItems().addAll("Cuidado facial", "Cuidado corporal", "Fragancias", "maquillaje",
				"capilar");
		categoriaComboBox.getSelectionModel().select("Cuidado facial");
	}

	@FXML
	void agregarProducto(MouseEvent event) {
		String nombreP = this.nombreTextField.getText();
		Producto nuevoProducto = new Producto(TipoCliente.values()[TipoComboBox.getSelectionModel().getSelectedIndex()],
				TipoProducto.values()[categoriaComboBox.getSelectionModel().getSelectedIndex()],
				nombreTextField.getText(), 
				Integer.parseInt(precioTextField.getText()),
				Integer.parseInt(cantidadTextField.getText()));
		Empresa maryQ = miVentanaPrincipal.getMariQ();
		try {
			maryQ.agregarProducto(nuevoProducto,
					miVentanaPrincipal.getMariQ().getSedes().get(comboSedes.getSelectionModel().getSelectedIndex()));
			this.miVentanaPrincipal.guardarEmpresa(this.miVentanaPrincipal.getMariQ(), this.miVentanaPrincipal.RUTA);
			JOptionPane.showMessageDialog(null,
					"Se agrego la cuidad correctamete al inventario Nacional" + " " + nuevoProducto);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			JOptionPane.showMessageDialog(null, e.getMessage());
		}

	}

	@FXML
	void actuaizarSede(MouseEvent event) {
		ArrayList<Sede> sedes = miVentanaPrincipal.getMariQ().getSedes();
		for (int i = 0; i < sedes.size(); i++) {
			ListaSedes.add(sedes.get(i).getNombreT());
		}
		comboSedes.setItems(ListaSedes);
	}

	public void setMainApp(Principal controladorCuidad) {
		this.miVentanaPrincipal = controladorCuidad;
	}

}
