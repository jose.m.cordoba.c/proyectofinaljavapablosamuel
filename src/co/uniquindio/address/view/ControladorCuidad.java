package co.uniquindio.address.view;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

import co.uniquindio.address.Principal;
import co.uniquindio.address.model.Cliente;
import co.uniquindio.address.model.Cuidad;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class ControladorCuidad implements Initializable{

	@FXML
	private TableView<Cuidad> tablaCuidades;

	@FXML
	private TableColumn<Cuidad, String> nombreCuidad = new TableColumn<Cuidad, String>("nombreC");
    
	
	private ObservableList<Cuidad> cuidades= FXCollections.observableArrayList();

	private Principal ventanaPrincipal;
	
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		nombreCuidad.setCellValueFactory(new PropertyValueFactory<>("nombreC"));
		tablaCuidades.setItems(cuidades);		
	}

	@FXML
	void agregarCuidad(MouseEvent event) {
		Node source = (Node) event.getSource();
		Stage stage1 = (Stage) source.getScene().getWindow();
		stage1.close();
		FXMLLoader loader = new FXMLLoader(this.getClass().getResource("/co/uniquindio/address/view/vistaNuevaCuidad.fxml"));
		try {
			ControladorNuevaCuidad control;
			Parent root = loader.load();
			control = loader.getController();
			control.setMiVentanaPrincipal(ventanaPrincipal);
			Scene scene = new Scene(root);
			Stage stage = new Stage();
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.setScene(scene);
			stage.show();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@FXML
	void BusarCuidadSede(MouseEvent event) {

	}

	@FXML
	void volver(MouseEvent event) {
		Node source = (Node) event.getSource();
		Stage stage1 = (Stage) source.getScene().getWindow();
		stage1.close();
		FXMLLoader loader = new FXMLLoader(this.getClass().getResource("/co/uniquindio/address/view/vistaAdministradorNacional.fxml"));
		try {
			ControladorAdministradorNacional control;
			Parent root = loader.load();
			control = loader.getController();
			control.setMiVentanaPrincipal(ventanaPrincipal);
			Scene scene = new Scene(root);
			Stage stage = new Stage();
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.setScene(scene);
			stage.show();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void setMiVentanaPrincipal(Principal miVentanaPrincipal) {
		this.ventanaPrincipal = miVentanaPrincipal;
	}
	
	public void actualizarTabla() {
		tablaCuidades.getItems().clear();
		if (ventanaPrincipal != null) {
			ArrayList<Cuidad>cuidad= this.ventanaPrincipal.getMariQ().getCuidades();
			for (int i = 0; i < cuidad.size(); i++) {
				cuidades.add(cuidad.get(i));
				System.out.println(cuidad.get(i));
			}
		}
	}
	
	@FXML
    void agregarSede(MouseEvent event) {
		FXMLLoader loader = new FXMLLoader(this.getClass().getResource("/co/uniquindio/address/view/vistaSedes.fxml"));
		try {
			ControladorSedes control;
			Parent root = loader.load();
			control = loader.getController();
			control.setMiVentanaPrincipal(ventanaPrincipal);
			Scene scene = new Scene(root);
			Stage stage = new Stage();
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.setScene(scene);
			stage.show();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }

	
/*
 * public void actualizarTabla() {
		tbDataP.getItems().clear();
		if (ventanaPrincipal != null) {
			ArrayList<ArrayList<Propietario>> matrizPro = ventanaPrincipal.getMiEstacionamiento()
					.obtenerPropietariosMasDe2VisitasFila();
			ArrayList<Propietario> propietarios = ventanaPrincipal.getMiEstacionamiento()
					.toArrayListListadoDePropietarios(matrizPro);
			for (int i = 0; i < propietarios.size(); i++)
				propietariosMasDosVeces.add(propietarios.get(i));
		}
	}
 * **/
}
