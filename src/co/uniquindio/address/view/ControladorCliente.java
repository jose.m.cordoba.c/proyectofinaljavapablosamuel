package co.uniquindio.address.view;

import java.io.IOException;

import co.uniquindio.address.Principal;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.input.MouseEvent;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class ControladorCliente {

	 private Principal ventanaPrincipal;

	public void setMiVentanaPrincipal(Principal principal) {
	        this.ventanaPrincipal=principal;			
			}
	
	@FXML
    void productos(MouseEvent event) {
		FXMLLoader loader = new FXMLLoader(this.getClass().getResource("/co/uniquindio/address/view/vistaProductosSede.fxml"));
		try {
			ControladorProductosCliente control;
			Parent root = loader.load();
			control = loader.getController();
			control.setMiVentanaPrincipal(ventanaPrincipal);
			Scene scene = new Scene(root);
			Stage stage = new Stage();
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.setScene(scene);
			stage.show();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }

    @FXML
    void revisarCompras(MouseEvent event) {

    }
    
    @FXML
    void volver(MouseEvent event) {
    	FXMLLoader loader = new FXMLLoader(this.getClass().getResource("/co/uniquindio/address/view/vistaEntrada.fxml"));
		try {
			ControladorEntrada control;
			Parent root = loader.load();
			control = loader.getController();
			control.setMiVentanaPrincipal(ventanaPrincipal);
			Scene scene = new Scene(root);
			Stage stage = new Stage();
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.setScene(scene);
			stage.show();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
    
}
