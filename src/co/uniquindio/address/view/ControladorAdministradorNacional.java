package co.uniquindio.address.view;

import java.awt.Button;
import java.awt.event.ActionEvent;
import java.io.IOException;

import javax.swing.JOptionPane;

import co.uniquindio.address.Principal;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.fxml.FXML;
import javafx.scene.control.TextField;

public class ControladorAdministradorNacional {

	private Stage escenarioPrincipal;

	@FXML
	private Principal miVentanaPrincipal;

	public void setMiVentanaPrincipal(Principal controladorEntrada) {
		this.miVentanaPrincipal = controladorEntrada;
	}

	@FXML
	public void initialize() {

	}

	@FXML
	void administrarAdministardores(MouseEvent event) {

	}

	@FXML
	void volver(MouseEvent event) {
		Node source = (Node) event.getSource();
		Stage stage1 = (Stage) source.getScene().getWindow();
		stage1.close();
		FXMLLoader loader = new FXMLLoader(
				this.getClass().getResource("/co/uniquindio/address/view/vistaEntrada.fxml"));
		try {
			ControladorEntrada control;
			Parent root = loader.load();
			control = loader.getController();
			control.setMiVentanaPrincipal(miVentanaPrincipal);
			Scene scene = new Scene(root);
			Stage stage = new Stage();
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.setScene(scene);
			stage.show();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@FXML
	void admisnstrarCuidad(MouseEvent event) {
		Node source = (Node) event.getSource();
		Stage stage1 = (Stage) source.getScene().getWindow();
		stage1.close();
		FXMLLoader loader = new FXMLLoader(this.getClass().getResource("/co/uniquindio/address/view/vistaCuidad.fxml"));
		try {
			ControladorCuidad control;
			Parent root = loader.load();
			control = loader.getController();
			control.setMiVentanaPrincipal(miVentanaPrincipal);
			Scene scene = new Scene(root);
			Stage stage = new Stage();
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.setScene(scene);
			stage.show();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@FXML
	void administarInventario(MouseEvent event) {
		FXMLLoader loader = new FXMLLoader(
				this.getClass().getResource("/co/uniquindio/address/view/vistaInventario.fxml"));
		try {
			ControladorInventario control;
			Parent root = loader.load();
			control = loader.getController();
			control.setMainApp(miVentanaPrincipal);
			Scene scene = new Scene(root);
			Stage stage = new Stage();
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.setScene(scene);
			stage.show();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
