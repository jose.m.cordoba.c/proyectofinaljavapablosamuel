package co.uniquindio.address.view;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.ResourceBundle;
import java.util.UUID;

import co.uniquindio.address.Principal;
import co.uniquindio.address.model.Cliente;
import co.uniquindio.address.model.Cuidad;
import co.uniquindio.address.model.Sede;
import co.uniquindio.address.model.Enum.TipoTarjeta;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.DatePicker;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.stage.Modality;
import javafx.stage.Stage;
public class ControladorNuevosClientes  implements Initializable{
	    

	    @FXML
	    private TextField DirrecionTextField;

	    @FXML
	    private TextField nombreTextField;
	    
	    @FXML
	    private TextField correoTextField;

	    @FXML
	    private TextField departamentoTextField;

	    @FXML
	    private TextField cuidadTextField;

	    @FXML
	    private TextField numeroTTextField;

	    @FXML
	    private ComboBox<String> comboCuidades;
		private ObservableList<String> listaCuidades = FXCollections.observableArrayList();		
	    
	    @FXML
	    private ComboBox<String> tipoTarjeta;
	    
	    @FXML
		 private ComboBox<String> comboSedes;
		 private ObservableList<String> listaSedes = FXCollections.observableArrayList();		

		 @FXML
		 private DatePicker nacimientoCliente;

		private Principal MiVentanaPrincipal;

		@Override
		public void initialize(URL location, ResourceBundle resources) {
			tipoTarjeta.getItems().removeAll(tipoTarjeta.getItems());
			tipoTarjeta.getItems().addAll("Debito ", "Credito");
			tipoTarjeta.getSelectionModel().select("Debito");
		}
		

	    @FXML
		void actualizarCuides(MouseEvent event) {
			listaCuidades.clear();
			ArrayList<Cuidad> Cuidades = MiVentanaPrincipal.getMariQ().getCuidades();
			for (int i = 0; i < Cuidades.size(); i++) {
				listaCuidades.add(Cuidades.get(i).getNombresC());
			}
			comboCuidades.setItems(listaCuidades);
			comboCuidades.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<String>() {
				public void changed(ObservableValue<? extends String> ov,
			            final String oldvalue, final String newvalue)
			    {
			      actualizarSedes(comboCuidades.getSelectionModel().getSelectedIndex());
			    }
	
			});
		}
	    
	    private void actualizarSedes(int selectedIndex) {
			listaSedes.clear();
			Cuidad cuidad= MiVentanaPrincipal.getMariQ().getCuidades().get(selectedIndex);
			ArrayList<Sede> Sedes= cuidad.obtenerSedes();
			for (int i = 0; i < Sedes.size(); i++) {
				listaSedes.add(Sedes.get(i).getNombreT());
			}
			comboSedes.setItems(listaSedes);
		}

		public void setMiVentanaPrincipal(Principal ventanaPrincipal) {
             this.MiVentanaPrincipal=ventanaPrincipal;			
		}
		
		 @FXML
		    void agregarCliente(MouseEvent event) {
			 String nombre=nombreTextField.getText();
			 Calendar fecha = Calendar.getInstance();
				fecha.clear();
				fecha.set(nacimientoCliente.getValue().getYear(), nacimientoCliente.getValue().getMonthValue() - 1,
						nacimientoCliente.getValue().getDayOfMonth());
			 Cliente cliente= new Cliente(UUID.randomUUID(), 
	        		 nombreTextField.getText(), 
	        		 DirrecionTextField.getText(), 
	        		 correoTextField.getText(), 
	        		 nacimientoCliente, 
	        		 departamentoTextField.getText(), 
	        		 cuidadTextField.getText(), 
	        		 tipoTarjeta.getSelectionModel().getSelectedIndex() == 0 ? tipoTarjeta.debito
	 						: tipoTarjeta.credito);
		    }

	    @FXML
	    void volver(MouseEvent event) {
	    	FXMLLoader loader = new FXMLLoader(this.getClass().getResource("/co/uniquindio/address/view/vistaEntrada.fxml"));
			try {
				ControladorEntrada control;
				Parent root = loader.load();
				control = loader.getController();
				control.setMiVentanaPrincipal(MiVentanaPrincipal);
				Scene scene = new Scene(root);
				Stage stage = new Stage();
				stage.initModality(Modality.APPLICATION_MODAL);
				stage.setScene(scene);
				stage.show();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	    }

	    @FXML
	    void clienteExistente(MouseEvent event) {
	    	FXMLLoader loader = new FXMLLoader(this.getClass().getResource("/co/uniquindio/address/view/vistaCliente.fxml"));
			try {
				ControladorCliente control;
				Parent root = loader.load();
				control = loader.getController();
				control.setMiVentanaPrincipal(MiVentanaPrincipal);
				Scene scene = new Scene(root);
				Stage stage = new Stage();
				stage.initModality(Modality.APPLICATION_MODAL);
				stage.setScene(scene);
				stage.show();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	    }
	}

