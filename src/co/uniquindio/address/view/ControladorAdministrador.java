package co.uniquindio.address.view;

import java.io.IOException;

import co.uniquindio.address.Principal;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.input.MouseEvent;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class ControladorAdministrador {
	private Principal ventanaPrincipal;

	@FXML
    void verVentas(MouseEvent event) {

    }

    @FXML
    void verInventario(MouseEvent event) {

    }

    @FXML
    void verClientes(MouseEvent event) {
    	Node source = (Node) event.getSource();
		Stage stage1 = (Stage) source.getScene().getWindow();
		stage1.close();
		FXMLLoader loader = new FXMLLoader(this.getClass().getResource("/co/uniquindio/address/view/vistaClienteS.fxml"));
		try {
			ControladorClienteS control;
			Parent root = loader.load();
			control = loader.getController();
			control.setMiVentanaPrincipal(ventanaPrincipal);
			Scene scene = new Scene(root);
			Stage stage = new Stage();
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.setScene(scene);
			stage.show();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

    @FXML
    void volver(MouseEvent event) {
    	Node source = (Node) event.getSource();
		Stage stage1 = (Stage) source.getScene().getWindow();
		stage1.close();
		FXMLLoader loader = new FXMLLoader(this.getClass().getResource("/co/uniquindio/address/view/vistaEntrada.fxml"));
		try {
			ControladorEntrada control;
			Parent root = loader.load();
			control = loader.getController();
			control.setMiVentanaPrincipal(ventanaPrincipal);
			Scene scene = new Scene(root);
			Stage stage = new Stage();
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.setScene(scene);
			stage.show();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }

    public void setMiVentanaPrincipal(Principal principal) {
        this.ventanaPrincipal=principal;			
		}

	
}
