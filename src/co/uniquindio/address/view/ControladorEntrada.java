package co.uniquindio.address.view;
import java.awt.Button;
import java.io.IOException;
import java.lang.reflect.Array;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

import javax.swing.JOptionPane;

import co.uniquindio.address.Principal;
import co.uniquindio.address.model.Cuidad;
import co.uniquindio.address.model.Sede;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.fxml.FXML;
import javafx.scene.control.TextField;

	public class ControladorEntrada {

	    @FXML
	    private Principal miVentanaPrincipal;
	    

	    @FXML
	    private ComboBox<String> comboCuidades;
		private ObservableList<String> listaCuidades = FXCollections.observableArrayList();		
		
		 @FXML
		 private ComboBox<String> comboSedes;
		 private ObservableList<String> listaSedes = FXCollections.observableArrayList();		


		 
		@FXML
		void actualizarCuides(MouseEvent event) {
			listaCuidades.clear();
			ArrayList<Cuidad> Cuidades = miVentanaPrincipal.getMariQ().getCuidades();
			for (int i = 0; i < Cuidades.size(); i++) {
				listaCuidades.add(Cuidades.get(i).getNombresC());
			}
			comboCuidades.setItems(listaCuidades);
			comboCuidades.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<String>() {
				public void changed(ObservableValue<? extends String> ov,
			            final String oldvalue, final String newvalue)
			    {
			      actualizarSedes(comboCuidades.getSelectionModel().getSelectedIndex());
			    }
	
			});
		}
		
		private void actualizarSedes(int selectedIndex) {
			listaSedes.clear();
			Cuidad cuidad= miVentanaPrincipal.getMariQ().getCuidades().get(selectedIndex);
			ArrayList<Sede> Sedes= cuidad.obtenerSedes();
			for (int i = 0; i < Sedes.size(); i++) {
				listaSedes.add(Sedes.get(i).getNombreT());
			}
			comboSedes.setItems(listaSedes);
		}
		
	    @FXML
	    void administardorNacional(MouseEvent event) {
	    	Node source = (Node) event.getSource();
	        Stage stage1 = (Stage) source.getScene().getWindow();
	        stage1.close();	
	    	FXMLLoader loader = new FXMLLoader(this.getClass().getResource("/co/uniquindio/address/view/vistaAdministradorNacional.fxml"));
				try {
					ControladorAdministradorNacional control;
					Parent root = loader.load();
					control = loader.getController();
		            control.setMiVentanaPrincipal(miVentanaPrincipal);
					Scene scene = new Scene(root);
					Stage stage = new Stage();
					stage.initModality(Modality.APPLICATION_MODAL);
					stage.setScene(scene);
					stage.show();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		    }
				
	    

	    @FXML
	    void administrador(MouseEvent event) {
	    	Node source = (Node) event.getSource();
	        Stage stage1 = (Stage) source.getScene().getWindow();
	        stage1.close();	
	    	FXMLLoader loader = new FXMLLoader(this.getClass().getResource("/co/uniquindio/address/view/vistaAdministrador.fxml"));
				try {
					ControladorAdministrador control;
					Parent root = loader.load();
					control = loader.getController();
		            control.setMiVentanaPrincipal(miVentanaPrincipal);
					Scene scene = new Scene(root);
					Stage stage = new Stage();
					stage.initModality(Modality.APPLICATION_MODAL);
					stage.setScene(scene);
					stage.show();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
	    }

	    @FXML
	    void cliente(MouseEvent event) {
	    	Node source = (Node) event.getSource();
	        Stage stage1 = (Stage) source.getScene().getWindow();
	        stage1.close();	
	    	FXMLLoader loader = new FXMLLoader(this.getClass().getResource("/co/uniquindio/address/view/vistaNuevoCiente.fxml"));
				try {
					ControladorNuevosClientes control;
					Parent root = loader.load();
					control = loader.getController();
		            control.setMiVentanaPrincipal(miVentanaPrincipal);
					Scene scene = new Scene(root);
					Stage stage = new Stage();
					stage.initModality(Modality.APPLICATION_MODAL);
					stage.setScene(scene);
					stage.show();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
	    }


		public void setMiVentanaPrincipal(Principal principal) {
           this.miVentanaPrincipal=principal;			
		}


	}

