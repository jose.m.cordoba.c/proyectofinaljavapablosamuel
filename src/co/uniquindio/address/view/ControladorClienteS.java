package co.uniquindio.address.view;
import java.io.IOException;

import javax.swing.JOptionPane;
import co.uniquindio.address.Principal;
import co.uniquindio.address.model.Cliente;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class ControladorClienteS {

    @FXML
    private TableColumn<Cliente,String > columanCuidad;
    
    @FXML
    private TableColumn<Cliente, String> ColumnaNombre;

    @FXML
    private TableColumn<Cliente, String> ColumanDocumento;

    @FXML
    private TableView<Cliente> tablaClientes;

    @FXML
    private TableColumn<Cliente,String > columanDepartamento;

    @FXML
    private TableColumn<Cliente,String > columanCorreo;
    private ObservableList<Cliente> cliente ;
    
    private Principal ventanaPrincipal;
    private	Stage dialogStage;
	private Stage stagePrincipal;

    public void setMiVentanaPrincipal(Principal principal) {
        this.ventanaPrincipal=principal;			
		}
    
    
    public void inicializar () {
    	cliente= FXCollections.observableArrayList();
    	for(Cliente clientes:ventanaPrincipal.buscarListaCliente()) {
    		cliente.add(clientes);
    	}
    	this.ColumnaNombre.setCellValueFactory(new PropertyValueFactory("Nombre"));
    	this.columanCuidad.setCellValueFactory(new PropertyValueFactory("Cuidad"));
    	this.ColumanDocumento.setCellValueFactory(new PropertyValueFactory("Documento"));
    	this.columanDepartamento.setCellValueFactory(new PropertyValueFactory("Departamento"));
    	this.columanCorreo.setCellValueFactory(new PropertyValueFactory("Correo"));
    	tablaClientes.setItems(cliente);
    }
    
    @FXML
    void AgregarCliente(MouseEvent event) {
		FXMLLoader loader = new FXMLLoader(this.getClass().getResource("/co/uniquindio/address/view/vistaNuevoCliente.fxml"));
		try {
			ControladorNuevosClientes control;
			Parent root = loader.load();
			control = loader.getController();
			control.setMiVentanaPrincipal(ventanaPrincipal);
			Scene scene = new Scene(root);
			Stage stage = new Stage();
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.setScene(scene);
			stage.show();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }

    @FXML
    void actualizarTabla(MouseEvent event) {
      
    }

    @FXML
    void volver(MouseEvent event) {
    	Node source = (Node) event.getSource();
		Stage stage1 = (Stage) source.getScene().getWindow();
		stage1.close();
		FXMLLoader loader = new FXMLLoader(this.getClass().getResource("/co/uniquindio/address/view/vistaAdministrador.fxml"));
		try {
			ControladorAdministrador control;
			Parent root = loader.load();
			control = loader.getController();
			control.setMiVentanaPrincipal(ventanaPrincipal);
			Scene scene = new Scene(root);
			Stage stage = new Stage();
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.setScene(scene);
			stage.show();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
    
}
