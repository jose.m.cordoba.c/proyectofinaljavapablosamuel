# Autodiagnostico

En cuanto al alcance del desarrollo del proyecto se alcanza un 68% de los 
requerimientos a nivel logico y a nivel de interfaz grafica alcanzamos un 90% 
de creacion de las interfaces graficas con un 80% funcionalidad.

## Metodos utilizados

Se crearon las clases modelo para el funcionamiento, de la aplicacion.

se crearon los sigientes metodos:
public void agregarAdministradorSede(Sede sede, Administrador administrador) {
		if (!administradoresSedes.contains(administrador)) {
			administradoresSedes.add(administrador);
		}
		sede.setAdministradorL(administrador);
	}

public void agregarSede(Sede sede, Cuidad cuidad) throws Exception {
		if (!cuidades.contains(cuidad)) {
			cuidades.add(cuidad);
		}
		cuidad.agregarSede(sede);
		sedes.add(sede);
	}

public void agregarCliente(Cliente cliente) throws Exception {
		if (!clientes.contains(cliente)) {
			clientes.add(cliente);
		}
	}

public void agregarProducto(Producto producto, Sede sede) throws Exception {
		if (!productos.contains(producto)) {
			productos.add(producto);
		}
		sede.agregarProducto(producto);
	}

public void agregarCuidad(Cuidad cuidad) {
		if (!cuidades.contains(cuidad)) {
			cuidades.add(cuidad);
		}
	}
	
Los metodos anteriormente mostrados, realizan la busqueda dentro de los arrayList, Como los productos o las sedes

-El administrador puede aumenta o disminuir la cantidad de existencias, de los productos dentro de la sede.

public void disminuirProducto(Producto producto, int cantidad) throws Exception {

if (inventarioS.contains(producto)) {
			int indice = inventarioS.indexOf(producto);// obtiene la pocion del producto
			int existencias = inventarioS.get(indice).getExistencias();
			if (existencias >= cantidad) {
				inventarioS.get(indice).setExistencias(existencias - cantidad);
			} else {
				inventarioS.get(indice).setExistencias(0);
			}
		} else {
			throw new Exception("Este preducto no existe");
		}
	}

public void aumentarProducto(Producto producto, int cantidad) throws Exception {
		if (inventarioS.contains(producto)) {
			int indice = inventarioS.indexOf(producto);// obtiene la pocion del producto
			int existencias = inventarioS.get(indice).getExistencias();
			inventarioS.get(indice).setExistencias(existencias + cantidad);
		} else {
			throw new Exception("Este preducto no existe");
		}

	}

 Estos metodos anteriormente mostrados, le permite al administrador aumentar o disminuir los productos dentro de una empresa
 

-El administrador le llega un correo con los productos con menos de 10 existencias

public ArrayList<Producto> notificarMenosDe10Existencias() {
		ArrayList<Producto> listaProducto = new ArrayList<Producto>();
		for (int i = 0; i < inventarioS.size(); i++) {
			if (inventarioS.get(i).getExistencias() < 10) {
				listaProducto.add(inventarioS.get(i));
			}
		}
		return listaProducto;
	}

El metodo anteriormente mostrado, busca los productos que tiene menos de los dies productos, el problema es 
la falta de enviar un correo al administrador avisando la falta de existencias

-un listado con las compras acomuladas, en un rango de de precio y tiempo

 Este metodo no se pudo realizar por la falta de tiempo y de logica.
 
 -Un listado con los productos mas vendidos, por existencia en el rango de tiempo especificado
 
 Este metodo se intento realizar con grandes fallos y poco exito en su implementacion.
 
 -El cliente con la compra del mes y enviarle un regalo de la tienda no superior a los 20000 pesos
 
 Este metodo tampoco se pudo realizar ni implementar dentro del programa
 
 -Un consolidado del dinero recaudado por mes, periodo y a�o.
 
 Este metodo por factores de tiempo, no se pudo completar.
 
 -Identificar las 5 sedes con mayores ventas
 
 Este se tenia planeado utilizar el metodo burbuja para buscar las cuidades con mayores ventas
 pero por factores de tiempo no se pudo completar. 
 
 -Para determinada sede, dando una fecha de inicio y de final, se debe de informar el dia en 
 el que mas se han realizado las compras mas costosas 
 
 El factor del tiempo influyo en la creacion de muchos de los metodos anterior mente mencionados.
 
 -Crear intervalos para diferentes productos, para informar cual es el producto mas vendido
 
 El poco conocimiento no permitio la creacion de los diferentes intervalos dentro de la vista de productos
 
 -mostrar el consolidado de dinero por sede y a nivel nacional
 
 Al faltar las ventas realizadas por las sedes no se puede crear el consolidado.
 
   -Cliente
   
   Cabe mencionar que la clase cliente y la clase compra fueron creados, pero no todas los metodos 
   fueron utilizados.
   
   -Visualizar el historico de compras, (Ordenado de lo mas reciente a lo mas actual)
   
    No se creo una clase historial, por lo tanto tener el historial de los productos mas 
    Comprados de los clientes no era posible.
    
   -Informacion del pedido
   
    Se intento realizar dicho metodo por la falta de tiempo no se pudieron realizar
    
   -Creacion de un reclamo y la creacion de un listado de favoritos
   
    Estos dos metodos se realcionan ya que al no tener los metodos anteriores estas no 
    pueden funcionar correctamente 
    
 -Concluciones 
 
 Aunque falten una gran cantidad de metodos, se compensa con la utilizacion de javaFx a nivel masivo dentro del proyecto, 
 Ademas de serealizar muchas arrayList como el de la empresa o los productos.
 
## Modelamiento y Concepto de  PROGRAMACION ORIENTADA A OBJETOS 

Se alcanza un 100% el objetivo de modelar una solucion utilizando el paradigma 
de programacion orientada a objetos, esto se evidencia en el diagrama de clases 
donde se pueden apreciar las interaciones entre las clases y sus intancias. De
esta forma cada clase representa una entidad independiente en los actores de 
la solucion propuesta con sus estados (Atributos) y acciones (metodos) claramente
definidos.

## Herencia

Se utiliza un 100% el concepto de herencia, por ejemplo, en la clase Persona
se definen las caracteristicas generales de un individuo, sin embargo, como pueden 
existir diferentes tipos de personas segun su rol (Cliente, Administrador) que
desempeñan diferentes acciones dentro del sistema se crean clases hijas para 
modelar dichos comportamientos.

## Polimorfismo

Se utiliza el concepto de polimorfismo entre las clases hijas donde algunas, 
utilizaron de manera polimorfica el constructor de la clase padre y ademas se 
alijaban en espacios de memoria diseñados para la clase padre

## Cohesion

Se posee una alta coesion ya que los nombres de las clases y los metodos son altamente descriptivos
y de igual forma los metodos de la clase no realizan metodos que no tienen que ver con ellas.

## Abstraccion

Todas las clases, son modelos que representan objetos en la vida real ademas de
la creacion de la clase persona con el metodo de la abstracion.

## Encapsulamiento 

Se alcanza el concepto de encapsulamiento al 100% al garantizar el acceso a los 
datos de manera exclusiva a traves de los medtodos dispuesto y de las clases
controladoras.

Un ejemplo que vale la pena resaltar es el agregar sede en la clase empresa
donde se garantiza la identidad del modelo de datos al encapsular la sede y
relacionarla a la ciudad.

## Acoplamiento

A nivel de modelo logico el acoplamiento es bajo y se logra un excelente rendimiento,
gracias a la adopcion de las buenas practicas asociadas al paradigma de programacion
orientada a objetos. A nivel de interfaz el nivel de acoplamiento es medio ya que
algunas partes del sistema dependen de otras para funcionar correctamente.

## Separacion de la logica y la interfaz

Se alcanza el 100% del objetivo de generar una separacion entre la logica y la
interfaz ya que estas son totalmente independientes y cuentan con un unico 
puente de comunicacion en el controlador de la empresa.